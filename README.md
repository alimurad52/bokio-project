## How to setup?
1. Clone the repository by running `git clone https://alimurad52@bitbucket.org/alimurad52/bokio-project.git` in your terminal.
2. run `npm install` in the project directory.
3. run `npm start`.

## Available routes
1. `/`
2. `/transactions`

## Available Scripts

`npm start`

`npm run build`

`npm run eject`

