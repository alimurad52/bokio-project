export const PARSE_DATA = "PARSE_DATA";
export const PARSE_ERROR = "PARSE_ERROR";
export const CLEAR_DATA = "CLEAR_DATA";
export const SAVE_DATA = "SAVE_DATA";
export const GET_DATA = "GET_DATA";
export const CHECK_DUPLICATE = "CHECK_DUPLICATE";
export const HAS_DUPLICATE = "HAS_DUPLICATE";
