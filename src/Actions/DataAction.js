import {CHECK_DUPLICATE, CLEAR_DATA, GET_DATA, HAS_DUPLICATE, PARSE_DATA, PARSE_ERROR, SAVE_DATA} from "./ActionType";
import axios from 'axios';
import data from '../Const/data';

export const parseData = (rows) => {
    return (dispatch) => {
        axios.post(data.baseURL + `/${data.api_key}/Transactions/Parse`, rows)
            .then((res) => {
                if(!res.data.suggestions || !res.data.suggestions.length) {
                    dispatch(errorMessage('Oops! Something is wrong with the data.'));
                    return false;
                }
                dispatch(setParsedData(res.data));
            })
            .catch((err) => {
                dispatch(errorMessage('Oops! Something went wrong. Please try again.'));
            })
    }
};

export const saveData = (rows) => {
    return (dispatch) => {
        axios.post(data.baseURL + `/${data.api_key}/Transactions`, rows)
            .then((res) => {
                if(res.data.length > 0 && res.status === 200) {
                    dispatch(setData(res.data));
                    dispatch(setClear());
                } else {
                    dispatch(errorMessage('Oops! Something went wrong. Please try again.'));
                }
            })
            .catch((err) => {
                dispatch(errorMessage('Oops! Something went wrong. Please try again.'));
            })
    }
};

export const getTransactions = () => {
    return (dispatch) => {
        axios.get(data.baseURL + `/${data.api_key}/Transactions`)
            .then((res) => {
                if(res.data && res.status === 200) {
                    if(res.data.length > 0) {
                        dispatch(setTransactionsData(res.data));
                    } else {
                        dispatch(setTransactionsData([]));
                    }
                }
            })
            .catch((err) => {
                dispatch(errorMessage('Oops! Something went wrong. Please try again.'));
            });
    }
};

export const checkDuplicates = (t) => {
    return (dispatch) => {
        axios.post(data.baseURL + `/${data.api_key}/Transactions/Preview`, {"transactions": t})
            .then((res) => {
                if(res.data && res.data.length > 0 && res.status === 200) {
                    let arr = [];
                    res.data.map((item) => {
                        let obj = item.transaction;
                        // if ignorable values exists set isDuplicate to true
                        obj['ignore'] = item.ignore;
                        if(item.ignore) {
                            dispatch(setHasDuplicate(true));
                        }
                        arr.push(obj);
                        return true;
                    });
                    dispatch(setDuplicate(arr));
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }
};

export const setDuplicateValue = (res) => {
    return (dispatch) => {
        dispatch(setHasDuplicate(res))
    }
};

export const clearData = () => {
    return (dispatch) => {
        dispatch(setClear());
    }
};

function setParsedData(res) {
    return {
        type: PARSE_DATA,
        payload: res
    }
}

function errorMessage(res) {
    return {
        type: PARSE_ERROR,
        payload: res
    }
}

function setClear() {
    return {
        type: CLEAR_DATA
    }
}

function setData(res) {
    return {
        type: SAVE_DATA,
        payload: res
    }
}

function setTransactionsData(res) {
    return {
        type: GET_DATA,
        payload: res
    }
}

function setDuplicate(res) {
    return {
        type: CHECK_DUPLICATE,
        payload: res
    }
}

function setHasDuplicate(res) {
    return {
        type: HAS_DUPLICATE,
        payload: res
    }
}
