import React from "react";
import Styles from '../Scss/main.module.scss';
import {connect} from 'react-redux';
import {checkDuplicates, parseData, setDuplicateValue} from "../Actions/DataAction";
import {toast} from "react-toastify";
import {SampleData} from "./SampleData";
import {prepareData} from "./ParsedData";

class DataParser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            showSample: false
        };
        this.onClickSubmit = this.onClickSubmit.bind(this);
        this.onClickExample = this.onClickExample.bind(this);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // in case of error update initiate the toast
        if(this.props.error !== prevProps.error && this.props.error !== null) {
            toast.error(this.props.error)
        }
        // in case of success parse initiate the toast
        if(this.props.data !== prevProps.data && this.props.data) {
            toast.success('Data successfully parsed.')
        }
        // check for duplicates when there are changes in data
        if(this.props.data !== prevProps.data && (!this.props.duplicated || !(this.props.duplicated.length > 0))) {
            if(this.props.data && this.props.data.length > 0) {
                let t = prepareData(true, this.props.data);
                this.props.checkDuplicates(t);
            }
        }
        // in case of save initiate the toast
        if(this.props.savedData !== prevProps.savedData && this.props.savedData) {
            toast.success('Data saved successfully');
        }
    }

    // onclick submit for parsing
    onClickSubmit() {
        if(this.state.data.length > 0) {
            this.props.parseData({"text":this.state.data});
            this.props.setDuplicateValue(false);
            this.setState({data: ''});
        }
    }

    // onclick to initiate the modal
    onClickExample(e) {
        e.preventDefault();
        this.setState(prevState => ({showSample: !prevState.showSample}))
    }

    render() {
        return (
            <div>
                <h3>Parse Data</h3>
                <small>Copy and paste a clip of your bank statement from your internet bank. Not sure how to make the clipboard? Look at an
                    <a href="#javascript" onClick={this.onClickExample}> example</a>
                </small>
                <textarea rows={8} placeholder="Paste transaction here..." className={`${Styles.marginTopBottom} ${Styles.textArea}`} value={this.state.data} onChange={(e) => this.setState({data: e.target.value})} />
                <input type='button' className={Styles.btn} value="Parse" onClick={this.onClickSubmit} />
                <div className={this.state.showSample ? Styles.displayBlock : Styles.displayNone}>
                    <SampleData onClick={this.onClickExample} />
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    let data = state.data.data.rows;
    let error = state.data.error;
    let savedData = state.data.savedData;
    let duplicated = state.data.duplicated;
    return {data, error, savedData, duplicated}
};
const mapDispatchToProps = dispatch => ({
    parseData: (data) => dispatch(parseData(data)),
    setDuplicateValue: (data) => dispatch(setDuplicateValue(data)),
    checkDuplicates: (data) => dispatch(checkDuplicates(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(DataParser)
