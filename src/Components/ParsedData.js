import React from "react";
import {connect} from "react-redux";
import Styles from "../Scss/main.module.scss";
import {clearData, saveData, checkDuplicates} from "../Actions/DataAction";
import { confirmAlert } from 'react-confirm-alert';
import {toast} from "react-toastify";

const ParsedData = (props) => {
    return (
        <div>
            <h3>Preview</h3>
            <h4 className={`${props.isDuplicate ? Styles.displayBlock : Styles.displayNone} ${Styles.error}`}><small>There are duplicates in the rows below.</small></h4>
            <table cellSpacing="0" className={`${Styles.fullWidth}`}>
                <thead>
                <tr className={props.isDuplicate ? Styles.displayNone : Styles.displayTableRow}>
                    {
                        props.suggestions ? props.suggestions.map((item, i) => {
                            return (
                                <th key={i}>{item.selectedOption.key}</th>
                            )
                        }) : <>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Amount</th>
                        </>
                    }
                </tr>
                <tr className={props.isDuplicate ? Styles.displayTableRow : Styles.displayNone}>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Ignore</th>
                </tr>
                </thead>
                <tbody>
                    {
                        props.data ? props.data.map((item, i) => {
                            return (
                                <tr key={i} className={props.isDuplicate ? Styles.displayNone : Styles.displayTableRow}>
                                    {
                                        item.cells.map((cells, k) => {
                                            return <td key={k}>{cells.cleanedValue}</td>
                                        })
                                    }
                                </tr>
                            )
                        }) : <tr className={props.isDuplicate ? Styles.displayNone : Styles.displayTableRow}><td>No data to show.</td></tr>
                    }
                    {
                        props.duplicated ? props.duplicated.map((item, i) => {
                            return (
                                <tr key={i} className={props.isDuplicate ? Styles.displayTableRow : Styles.displayNone}>
                                    <td>{item.date}</td>
                                    <td>{item.text}</td>
                                    <td>{item.amount}</td>
                                    <td>{item.ignore ? "True" : "False"}</td>
                                </tr>
                            )
                        }) : <tr className={props.isDuplicate ? Styles.displayTableRow : Styles.displayNone}><td>No data to show.</td></tr>
                    }
                </tbody>
            </table>
            <input type='button' className={`${Styles.btn} ${Styles.marginTopBottom}`} value="Clear Data" onClick={props.clearData} />
            <input type="button" className={`${Styles.btn} ${Styles.marginLeftRight}`} value="Save" onClick={() => onClickSave(props.isDuplicate, props.data, props)} />
        </div>
    )
};

// onclick save transactions
const onClickSave = (hasDuplicates, data, props) => {
    let rows = props.data || props.duplicated;
    if(rows.length > 0) {
        if(hasDuplicates) {
            let row = props.duplicated;
            // check if has duplicates and show the alert modal accordingly
            confirmAlert({
                title: 'Duplicates',
                message: 'There are duplicate rows in the parsed data. Do you wish to keep the duplicates or ignore?',
                buttons: [
                    {
                        label: 'Keep',
                        onClick: () => {
                            // pass all values to save
                            props.saveData(row);
                        }
                    },
                    {
                        label: 'Ignore',
                        onClick: () => {
                            //check for the ignored values and only pass the false ones
                            let arr = [];
                            row.map((item) => {
                                if(!item.ignore) {
                                    arr.push(item);
                                }
                                return true;
                            });
                            if(arr.length > 0) {
                                props.saveData(arr);
                            } else {
                                toast.success('No data to save.');
                                props.clearData();
                            }
                        }
                    }
                ]
            });
        } else {
            // prepare data to pass to api
            let transaction = prepareData(true, rows);
            props.saveData(transaction);
        }
    }
};

// map data to create object that backend accepts
export const prepareData = (keep, rows) => {
    let arr = [];
    rows.map((item, i) => {
        let obj = {};
        // check ech string validation
        item.cells.map((cell) => {
            if(cell.columnType === 2 && !obj['date']) {
                obj['date'] = cell.cleanedValue;
            }
            if(cell.columnType === 0 && !obj['text']) {
                obj['text'] = cell.cleanedValue;
            }
            if(cell.columnType === 1 && !obj['amount']) {
                obj['amount'] = parseFloat(cell.cleanedValue.replace(',','.').replace(' ',''));
            }
            return false;
        });
        arr.push(obj);
        return false;
    });
    return arr;
};

const mapDispatchToProps = dispatch => ({
    clearData: () => dispatch(clearData()),
    saveData: (data) => dispatch(saveData(data)),
    checkDuplicates: (data) => dispatch(checkDuplicates(data))
});

const mapStateToProps = (state) => {
    let data = state.data.data.rows;
    let res = state.data.data;
    let suggestions = state.data.data.suggestions;
    let duplicated = state.data.duplicated;
    let isDuplicate = state.data.isDuplicate;
    return {data, res, suggestions, duplicated, isDuplicate}
};

export default connect(mapStateToProps, mapDispatchToProps)(ParsedData)
