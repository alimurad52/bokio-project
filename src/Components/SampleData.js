import React from 'react';
import Styles from '../Scss/main.module.scss';
import data from '../Const/data';

export const SampleData = ({onClick}) => {
    return (
        <div className="modal">
            <div className="modal-content">
                <span className="close" onClick={onClick}>&times;</span>
                <div className={Styles.marginTopBottom}>
                    <h3>Sample Data</h3>
                    <p><small>You may copy (cmd + a and cmd + c) sample data from below and paste (cmd + v) in the parser to test.</small></p>
                    <table cellSpacing="0" className={Styles.fullWidth}>
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Description</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            data.sampleData.map((item) => {
                                return <tr key={item.id}>
                                    <td>{item.date}</td>
                                    <td>{item.text}</td>
                                    <td>{item.amount.toFixed(2)}</td>
                                </tr>
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    )
};
