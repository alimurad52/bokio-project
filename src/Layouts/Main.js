import React from 'react';
import DataParser from "../Components/DataParser";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ParsedData from "../Components/ParsedData";
import Styles from '../Scss/main.module.scss';
import 'react-confirm-alert/src/react-confirm-alert.css';

const Main = () => {
    return (
        <div className={Styles.customContainer}>
            <div className={Styles.fullWidth}>
                <h4 className={Styles.floatLeft}>Import from bank</h4>
                <input type="button" value="History" className={`${Styles.btn} ${Styles.floatRight}`} onClick={ () => window.routerHistory.push("/transactions")} />
            </div>
            <div className={Styles.clearBoth}><DataParser/></div>
            <ParsedData />
            <ToastContainer />
        </div>
    )
};

export default Main;
