import React from "react";
import Styles from '../Scss/main.module.scss';
import {getTransactions} from "../Actions/DataAction";
import {connect} from "react-redux";

class TransactionsPage extends React.Component {
    componentDidMount() {
        // get past transactions
        this.props.getTransactions();
    }

    render() {
        return (
            <div className={Styles.customContainer}>
                <input type="button" className={`${Styles.btn} ${Styles.marginTopBottom} ${Styles.floatLeft}`} value="Refresh" onClick={this.props.getTransactions} />
                <input type="button" className={`${Styles.btn} ${Styles.marginTopBottom} ${Styles.floatRight}`} value="Go Back" onClick={() => window.routerHistory.push('/')} />
                <p className={Styles.clearBoth}>It may take sometime for the API to return the updated list. Please try refreshing again if you don't see the expected data.</p>
                <table cellSpacing="0" className={`${Styles.fullWidth} ${Styles.clearBoth}`}>
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.transactions ? this.props.transactions.length > 0 ? this.props.transactions.map((item, i) => {
                            return (
                                <tr key={i}>
                                    <td>{item.date}</td>
                                    <td>{item.text}</td>
                                    <td>{item.amount}</td>
                                </tr>
                            )
                        }) : <tr><td>No data to show.</td></tr> : <tr><td>No data to show.</td></tr>
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    getTransactions: () => dispatch(getTransactions())
});

const mapStateToProps = (state) => {
    let transactions = state.data.transactions;
    return {transactions}
};

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsPage)
