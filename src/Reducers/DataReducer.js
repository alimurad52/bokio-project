import {
    CHECK_DUPLICATE,
    CLEAR_DATA,
    GET_DATA,
    HAS_DUPLICATE,
    PARSE_DATA,
    PARSE_ERROR,
    SAVE_DATA
} from "../Actions/ActionType";

const INITIAL_STATE = {
    data: [],
    error: null,
    isDuplicate: false,
    savedData: [],
    transactions: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case PARSE_DATA:
            return Object.assign({}, state, {
                data: action.payload,
                error: null,
                duplicated: []
            });
        case PARSE_ERROR:
            return Object.assign({}, state, {
                error: action.payload
            });
        case CLEAR_DATA:
            return Object.assign({}, {
                error: null,
                data: [],
                duplicated: []
            });
        case SAVE_DATA:
            return Object.assign({}, {
                ...state,
                savedData: action.payload
            });
        case GET_DATA:
            return Object.assign({}, {
                ...state,
                transactions: action.payload
            });
        case CHECK_DUPLICATE:
            return Object.assign({}, {
                ...state,
                duplicated: action.payload
            });
        case HAS_DUPLICATE:
            return Object.assign({}, {
                ...state,
                isDuplicate: action.payload
            });
        default:
            return state;
    }
}
