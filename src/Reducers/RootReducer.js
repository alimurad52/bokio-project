import { combineReducers } from 'redux';
import dReducer from './DataReducer';

//combining the reducers here for this reducer is exported to store
//to create the props in local storage
const rootReducer = combineReducers({
    data: dReducer
});

export default rootReducer;
