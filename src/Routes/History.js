import { createBrowserHistory } from 'history';
// create a browser history object to push a route from anywhere in the project
export default createBrowserHistory();
