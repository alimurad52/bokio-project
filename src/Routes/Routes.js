import { Router, Route, withRouter } from "react-router-dom";
import history from './History';
import Main from "../Layouts/Main";
import React from 'react';
import TransactionsPage from "../Layouts/TransactionsPage";

const base_url = process.env.PUBLIC_URL;
//assigning history to window for pushing any route if needed
window.routerHistory = history;

const Routes = () => {
    return (
    <Router history={history}>
        <Route exact path={`${base_url}/`} component={withRouter(Main)} />
        <Route exact path={`${base_url}/transactions`} component={withRouter(TransactionsPage)} />
    </Router>
)
};

export default Routes;
